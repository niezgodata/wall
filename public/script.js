/****************************************************************
*
* Changer la durée ici (millisecondes)
*
*/

var duration = 30000;

/*
 *
 * ************************************************************/


var sects = document.getElementsByTagName("section");
var num = sects.length;

var ctn = 0;

var gos = [
	document.getElementById("go-build"),
	document.getElementById("go-elec"),
	document.getElementById("go-eau"),
	document.getElementById("go-recy")
]



function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

var foo = getParameterByName("u");
var timer = null;

function gotosection (n) {

	sects[ctn].classList.add("out");
	sects[ctn].classList.remove("active");
	gos[ctn].classList.remove("active");

	n = (n+1)%num;

	gos[n].classList.add("active");
	sects[n].classList.add("active");
	sects[n].classList.remove("out");

	return n;
}

function jumptosection (n) {

	sects[ctn].classList.add("out");
	sects[ctn].classList.remove("active");
	gos[ctn].classList.remove("active");

	gos[n].classList.add("active");
	sects[n].classList.add("active");
	sects[n].classList.remove("out");

	clearInterval(timer);
	timer = setInterval( function () {

		ctn = gotosection(ctn);

	}, duration);


	return n;
}

function start () {

	for (var k=0;k<gos.length;k++){
		gos[k].addEventListener("click", function(event){
			var pos = Array.from(event.target.parentNode.children).indexOf(event.target);
			var len = event.target.parentNode.children.length
			console.log(len);
			console.log(pos);
			ctn = jumptosection(len-pos-1);
		});
	}

	timer = setInterval( function () {

		ctn = gotosection(ctn);

	}, duration);


}

if (!foo){

	start();

} else {

	switch (foo.toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "")) {
		case "batiment":

			break;
		case "electricite":
			sects[ctn].classList.add("out");
			sects[ctn].classList.remove("active");
			gos[ctn].classList.remove("active");

			ctn = 1;

			gos[ctn].classList.add("active");
			sects[ctn].classList.add("active");
			sects[ctn].classList.remove("out");
			break;
		case "eau":
			sects[ctn].classList.add("out");
			sects[ctn].classList.remove("active");
			gos[ctn].classList.remove("active");

			ctn = 2;

			gos[ctn].classList.add("active");
			sects[ctn].classList.add("active");
			sects[ctn].classList.remove("out");
			break;
		case "dechets":
			sects[ctn].classList.add("out");
			sects[ctn].classList.remove("active");
			gos[ctn].classList.remove("active");

			ctn = 3;

			gos[ctn].classList.add("active");
			sects[ctn].classList.add("active");
			sects[ctn].classList.remove("out");
			break;
		default:


			start();
			break;
	}
}
